This program is a Conway's Game of Life implementation in Java written by The University of Toledo Programming Club.
Conway's Game of Life is supposed to be played on an infinit two-dimensional orthoganal grid of square cells, but that's just a little bit too hard.
Conway's Game of Life is defined by the following rules:
    Any live cell with fewer than two live neighbors dies.
    Any live cell with two or three live neighbors lives on to the next generation.
    Any live cell with more than three live neighbors dies.
    Any dead cell with exactly three live neighbors becomes a live cell.
