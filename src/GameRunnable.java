import javafx.application.Platform;

class GameRunnable implements Runnable {
    private Thread t;
    private String threadName;
    private GameOfLife main;
   
    public GameRunnable(String name, GameOfLife main) {
        this.threadName = name;
        this.main = main;
        System.out.println("Creating " +  threadName );
    }
   
    public void run() {
		Platform.runLater(new Runnable() {
			@Override 
			public void run() {
				while (main.isRunning) {
					main.GoL.step();
					main.updateGrid();
				}
			}
		});
    }
   
   public void start() {
      System.out.println("Starting " +  threadName );
      if (t == null) {
         t = new Thread (this, threadName);
         t.start();
      }
   }
}