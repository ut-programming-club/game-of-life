import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.stage.Stage;


public class GameOfLife extends Application {
	GridPane grid;
	Button btnRun, btnStep;
	Boolean isRunning;
	Game GoL;
	GameRunnable gameThread;
	int size = 40;
	int cellSize = 10;
	int spacing = 2;
	
	@Override
	public void start(Stage primaryStage) {
		GoL = new Game(size);
		isRunning = false;
		gameThread = new GameRunnable("GoL Thread", this);

		grid = new GridPane();
		grid.setAlignment(Pos.CENTER);
		grid.setHgap(spacing);
		grid.setVgap(spacing);
		
		grid.setOnMouseClicked(e -> {
			handleClick(e);
		});
	
		for (int i = 0; i < size; i++) {
			for (int j = 0; j < size; j++) {
				Rectangle rect = new Rectangle(cellSize, cellSize);
				rect.setFill(Color.WHITE);
				grid.add(rect, j, i);
			}
		}
		
		GridPane buttons = new GridPane();
		buttons.setAlignment(Pos.CENTER);
		buttons.setPadding(new Insets(5, 0, 0, 0));
		buttons.setHgap(10);
		
		btnRun = new Button("Run");
		btnStep = new Button("Step");
		
		btnRun.setOnAction(e -> {
			handleRun(e);
		});
		btnStep.setOnAction(e -> {
			handleStep(e);
		});
		
		buttons.add(btnRun, 0, 0);
		buttons.add(btnStep, 1, 0);
		
		BorderPane mainPane = new BorderPane();
		mainPane.setCenter(grid);
		mainPane.setBottom(buttons);
		mainPane.setStyle("-fx-background-color: darkgrey;");
		mainPane.setPadding(new Insets(10, 10, 5, 10));

		Scene scene = new Scene(mainPane);
		
		primaryStage.setTitle("Conway's Game of Life");
		primaryStage.setScene(scene);
		primaryStage.show();
	}

	public void handleRun(ActionEvent event) {
		if (isRunning) {
			isRunning = false;
			btnRun.setText("Run");
		} else {
			isRunning = true;
			btnRun.setText("Stop");
			
			gameThread.start();
		}
	}
	
	public void handleStep(ActionEvent event) {
		if (!isRunning) {
			GoL.step();
			updateGrid();
		}
	}
	
	public void handleClick(MouseEvent event) {
		System.out.println("Not done yet " + event.getX() + ", " + event.getY());
	}
	
	public void updateGrid() {
		boolean[][] GolGrid = GoL.returnGrid();
		for (int i = 0; i < size; i++) {
			for (int j = 0; j < size; j++) {
				Rectangle rect = new Rectangle(cellSize, cellSize);
				if(GolGrid[i][j] == true){
					rect.setFill(Color.BLACK);
				}
				else{
				rect.setFill(Color.WHITE);
				}
				grid.add(rect, j, i);
			}
		}
	}
	
	public static void main(String[] args) {
		Application.launch(args);
	}

}
