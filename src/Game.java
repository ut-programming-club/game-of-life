//representation of Conway's Game of Life. Top and bottom "wrap around," as do edges.
public class Game {
	private boolean[][][] grid;
//	private boolean[][] nextState;
	private int size;
	boolean currGrid;
	
	public Game(int size){
		this.size = size;
		grid = new boolean[2][size][size];
		currGrid = true;
//		nextState = new boolean[size][size];
	}
	
	
	//Flips the value of a cell. Called by the GUI when user clicks on the given cell.
	public void flipCell(int x, int y){
		int i;
		if(currGrid)
			i = 0;
		else
			i = 1;
		grid[i][x][y] = !(grid[i][x][y]);
	}
	
	public boolean[][] returnGrid(){
		int i;
		if(currGrid)
			i = 0;
		else
			i = 1;
		return grid[i];
	}
	
	
	public void step(){
		int k;
		if(currGrid) // k gives the opposite grid that currGrid represents
			k = 1;
		else 
			k = 0;
		
		for (int i = 0; i < size; i++){
			for( int j = 0; j < size; j++){
				grid[k][i][j] = fateOfCell(i , j);
			}
		}
		currGrid = !currGrid;
		
	}
	
/*	
	public void step(){
		for (int i = 0; i < size; i++){
			for( int j = 0; j < size; j++){
				nextState[i][j] = fateOfCell(i , j);
			}
		}
		for (int i = 0; i < size; i++){
			for( int j = 0; j < size; j++){
				 grid[i][j] = nextState[i][j];
        	}
    	}
	}
*/
	
	
	//Determines whether the cell in position (x,y) lives or dies in this iteration.
	//life is represented with "true" and death with "false"
	private boolean fateOfCell(int x, int y){ 
		int adj = numAdjacentCells(x,y); 
		
		int i;
		if(currGrid)
			i = 0;
		else
			i = 1;
		
		if (grid[i][x][y]){
			if ((adj == 2 ) || (adj == 3 )) {
				return true;
			}
			else {
				return false;
			}
		}
		else {
			if (adj == 3) {
				return true;
			}
			else {
				return false;
			}
		}
    }
	
	private int numAdjacentCells(int x, int y){
		int i;
		if(currGrid)
			i = 0;
		else
			i = 1;
		int adjacent = 0;
		if (grid[i][((x-1)%size + size)%size][((y-1)%size + size)%size]) {
		    adjacent++;
		}
		if (grid[i][((x-1)%size + size)%size][(y)%size]) {
		    adjacent++;
		}
		if (grid[i][((x-1)%size + size)%size][(y+1)%size]) {
		    adjacent++;
		}
		if (grid[i][(x)%size][((y-1)%size + size)%size]) {
		    adjacent++;
		}
		if (grid[i][(x)%size][(y+1)%size]) {
		    adjacent++;
		}
		if (grid[i][(x+1)%size][((y-1)%size + size)%size]) {
		    adjacent++;
		}
		if (grid[i][(x+1)%size][(y)%size]) {
		    adjacent++;
		}
		if (grid[i][(x+1)%size][(y+1)%size]) {
		    adjacent++;
		}
		return adjacent;
	}
	
}